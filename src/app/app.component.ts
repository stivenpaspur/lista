import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PageEvent } from '@angular/material/paginator';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'server';
  articulos : any;
  pageSize=5;
  desde: number=0;
  hasta:number=10;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get("https://www.datos.gov.co/resource/4din-bcuq.json")
      .subscribe(
        result => {
          this.articulos = result;
        },
        error => {
          console.log('problemas');
        }
      );
  }
  cambiarpag(e:PageEvent){
    this.desde = e.pageIndex * e.pageSize;
    this.hasta = this.desde + e.pageSize;
  }
}
